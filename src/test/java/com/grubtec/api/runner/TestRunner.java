package com.grubtec.api.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = {"src/test/java/com/grubtec/api/features"},
        glue = {"com.grubtec.api.base", "com.grubtec.api.step_definitions"},
        format = {"pretty", "json:target/cucumber.json"},
        tags = {"@Smoke"},
        monochrome = true
)
public class TestRunner {
}
